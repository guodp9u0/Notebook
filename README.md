# Notebook

## 系统

* [ ] [计算机系统](https://github.com/dp9u0/Notebook-CSAPP)
* [ ] [操作系统](https://github.com/dp9u0/Notebook-OS)
* [ ] [计算网络](https://github.com/dp9u0/Notebook-Network)
* [ ] [算法与数据结构](https://github.com/dp9u0/Notebook-Algorithm)
* [ ] 编译原理

## 语言

* [X] [Javascript](https://github.com/dp9u0/Notebook-Javascript) : v1.0.0
* [ ] [Java](https://github.com/dp9u0/Notebook-Java)
* [ ] [Shell](https://github.com/dp9u0/Notebook-Shell)
* [ ] [DotNet](https://github.com/dp9u0/Notebook-DotNet)

## 软工

* [ ] [设计模式](https://github.com/dp9u0/Notebook-DesignPattern)
* [ ] [前端架构](https://github.com/dp9u0/Notebook-FrontEnd)

## 应用

* [X] [Git](https://github.com/dp9u0/Notebook-Git) : v1.0.0
* [ ] Windbg
